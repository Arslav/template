<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
//        'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap',
//        '/vendor/bootstrap/css/bootstrap-custom.min.css',
//        '/vendor/slick/slick.css',
//        '/vendor/slick/slick-theme.css',
//        '/vendor/fancybox/jquery.fancybox.min.css',
//        '/css/helper-classes.min.css',
//        '/css/main.css',
    ];

    public $js = [
        //'/vendor/jquery/jquery-3.3.1.min.js',
        //'/vendor/bootstrap/js/bootstrap.min.js',
//        '/vendor/slick/slick.min.js',
//        '/vendor/mask/jquery.mask.min.js',
//        '/vendor/object-fit/ofi.min.js',
//        '/vendor/fancybox/jquery.fancybox.min.js',
//        '/vendor/smoothscrollbar/smooth-scrollbar.js',
//        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
//        //'/vendor/yandex.js',
//        '/js/main.js',
//        '/vendor/parallax/parallax.js',
//        '/vendor/rellax.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset', // вместо BootstrapAsset для того, чтобы всегда подключался bootstrap.js
    ];
}
