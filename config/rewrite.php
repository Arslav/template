<?php
return array(
	// backend #############################################################################
	'admin-login' => 'user/admin-login',

	'admin/<controller:\w+>/<id:\d+>' => 'admin/<controller>/view',
	'admin/<controller:\w+>/<action:\w+>/<id:\d+>' => 'admin/<controller>/<action>',
	'admin/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',

	// frontend ############################################################################
	'success' => 'site/success',

	'sitemap.xml' => 'site/sitemap',

	'page/<slug:[a-z0-9–\-\s]+>' => 'page/view',

    'admin/index' => 'admin/default/index',
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',

);