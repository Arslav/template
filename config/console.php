<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'settings' => [
            'class' => 'yii2mod\settings\components\Settings',
        ],
    ],
    'params' => $params,
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => dirname(__DIR__).'\migrations\templates\red-promo\migration.php',
            'generatorTemplateFiles' => [
                'create_table' => dirname(__DIR__).'\migrations\templates\red-promo\createTableMigration.php',
                'drop_table' => dirname(__DIR__).'\migrations\templates\red-promo\dropTableMigration.php',
                'add_column' => dirname(__DIR__).'\migrations\templates\red-promo\addColumnMigration.php',
                'drop_column' => dirname(__DIR__).'\migrations\templates\red-promo\dropColumnMigration.php',
                'create_junction' => dirname(__DIR__).'\migrations\templates\red-promo\createTableMigration.php'
            ]
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
