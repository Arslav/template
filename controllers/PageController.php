<?php

namespace app\controllers;

use Yii;
use app\backend\components\FrontController;
use app\models\Page;

class PageController extends FrontController
{
    
    public $layout = 'main';


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }
    
    public function actionView($slug)
    {
        $model = Page::findBySlug($slug);
        
        return $this->render($model->template, compact('model'));
    }

}
