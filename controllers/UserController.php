<?php

namespace app\controllers;

use app\backend\components\FrontController;
use app\components\UnisenderHelper;
use app\models\AgeCategory;
use app\models\Child;
use app\models\ProfileMenuItem;
use app\models\ResetPasswordForm;
use app\models\SignUpForm;
use app\models\Social;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class UserController extends FrontController
{
    public $layout = 'secondary';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionAdminLogin()
    {
        
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('admin/contact');
        }

        $model = new LoginForm();
        $model->check = 19; //TODO: Скрипта проверки JS нет, по этмоу присваиваем знаение вручную.
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
            return $this->redirect('admin/contact');
        }

        $model->password = '';
        return $this->render('admin-login', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionSignUp($ref = null){
        $this->layout = 'auth';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/profile']);
        }

        $model = new SignUpForm();
        if ($model->load(Yii::$app->request->post()) && $model->signUp()) {
            Yii::$app->session->setFlash('success', 'Вам отправлено письмо для подтверждения аккаунта Пройдите по ссылке в письме, чтобы завершить регистрацию');
        }else{
            if($ref != null && $user = User::findOne(['invite_token' => $ref])) {
                Yii::$app->session->setFlash('success', 'Пользователь '.$user->name.' приглашает Вас зарегистрироваться!');
            }
        }
        return $this->render('sign-up', compact('model'));
    }

    /**
     * @return string|Response
     */
    public function actionLogin($validated = false){
        $this->layout = 'auth';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/profile']);
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/profile']);
        }
        $model->password = '';
        return $this->render('user-login', compact('model', 'validated'));
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/']);
    }

    /**
     * @param $token
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionValidate($token)
    {
        $user = User::findOne(['validation_token' => $token]);
        if($user){
            $user->status = User::STATUS_ACTIVE;
            $user->validation_token = null;
            $user->save();
//            $unisenderHelper = new UnisenderHelper();
//            $unisenderHelper->sendWelcomeMessage($user->email, $user->name);
            Yii::$app->session->setFlash('success', 'Регистрация прошла успешно!');
            return $this->redirect(['user/login', 'validated' => '1']);
        }
        return $this->redirect(['/']);
    }

    /**
     * @return string
     */
    public function actionForgotPassword()
    {
        $this->layout = 'auth';
        if($email = Yii::$app->request->post('email')) {
            Yii::$app->session->setFlash('success','Сообщение отправлено!');
            $user = User::findByEmail($email);
            if($user && $user->status == User::STATUS_ACTIVE){
                try {
                    $user->password_reset_token = Yii::$app->security->generateRandomString();
                    $user->save();
                    Yii::$app->mailer->compose('reset-password', ['user' => $user])
                        ->setTo($user->email)
                        ->setFrom([Yii::$app->settings->get('SiteSettings', 'fromEmail') => 'Талантикум'])
                        ->setSubject('Сброс пароля')
                        ->send();
                } catch (\Exception $exc) {
                    Yii::error($exc);
                }
            }
        }
        return $this->render('forgot-password', compact('email'));
    }

    /**
     * @param $token
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'auth';
        $model = new ResetPasswordForm();
        $user = User::find()->where(['password_reset_token' => $token])->one();
        if(!$user) throw new NotFoundHttpException();
        $model->user = $user;
        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            Yii::$app->session->setFlash('success','Пароль успешно изменен');
            return $this->redirect(['user/login']);
        }
        return $this->render('reset-password',compact('model','token'));
    }
}
