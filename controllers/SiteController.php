<?php

namespace app\controllers;

use app\models\AboutSlide;
use app\models\Apartment;
use app\models\Coordinates;
use app\models\Document;
use app\models\GalleryItem;
use app\models\Page;
use app\models\Place;
use app\models\PlaceSlide;
use app\models\Slide;
use app\models\Video;
use Yii;
use app\backend\components\FrontController;

class SiteController extends FrontController
{
    public $layout = 'main';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'main',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionSuccess()
    {
        return $this->render('success');
    }

    /**
     * Генерация файла sitemap.xml
     * @return string
     */
    public function actionSitemap()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');

        $sitemap['page'] = Page::find()->select(['slug', 'created_at', 'updated_at'])->asArray()->all();

        return $this->renderPartial('sitemap', ['sitemap' => $sitemap, 'host' => Yii::$app->request->hostInfo]);
    }
}
