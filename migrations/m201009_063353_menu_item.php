<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063353_menu_item extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%menu_item}}',
            [
                'id'=> $this->primaryKey(11),
                'parent_id'=> $this->integer(11)->null()->defaultValue(null),
                'title'=> $this->string(250)->notNull(),
                'visible'=> $this->boolean()->notNull()->defaultValue(1),
                'type'=> $this->string(250)->null()->defaultValue(null),
                'reference'=> $this->string(250)->null()->defaultValue(null),
                'link'=> $this->string(250)->null()->defaultValue(null),
                'sort'=> $this->integer(11)->notNull()->defaultValue(0),
            ],$tableOptions
        );
        $this->createIndex('parent_id','{{%menu_item}}',['parent_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('parent_id', '{{%menu_item}}');
        $this->dropTable('{{%menu_item}}');
    }
}
