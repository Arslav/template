<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063401_user_log extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%user_log}}',
            [
                'id'=> $this->primaryKey(11),
                'type'=> $this->string(50)->null()->defaultValue(null),
                'record_id'=> $this->integer(11)->null()->defaultValue(null),
                'table'=> $this->string(50)->null()->defaultValue(null),
                'column'=> $this->string(50)->null()->defaultValue(null),
                'old_value'=> $this->string(50)->null()->defaultValue(null),
                'new_value'=> $this->string(50)->null()->defaultValue(null),
                'user_id'=> $this->integer(11)->notNull(),
                'created_at'=> $this->integer(11)->notNull()->defaultValue(0),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%user_log}}');
    }
}
