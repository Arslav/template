<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063357_setting extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%setting}}',
            [
                'id'=> $this->primaryKey(11),
                'type'=> $this->string(10)->notNull(),
                'section'=> $this->string(255)->notNull(),
                'key'=> $this->string(255)->notNull(),
                'value'=> $this->text()->notNull(),
                'status'=> $this->smallInteger(6)->notNull()->defaultValue(1),
                'description'=> $this->string(255)->null()->defaultValue(null),
                'created_at'=> $this->integer(11)->notNull(),
                'updated_at'=> $this->integer(11)->notNull(),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%setting}}');
    }
}
