<?php

/**
 * Creates a call for the method `yii\db\Migration::createTable()`.
 */
/* @var $table string the name table */
/* @var $fields array the fields */
/* @var $foreignKeys array the foreign keys */

$generate_visible = true;
$generate_sort = true;
$generate_created_at = true;
$generate_updated_at = true;

?>        $this->createTable('<?= $table ?>', [
<?php foreach ($fields as $field):
    switch ($field['property'])
    {
        case 'visible': $generate_visible = false; break;
        case 'sort': $generate_sort = false; break;
        case 'created_at': $generate_created_at = false; break;
        case 'updated_at': $generate_updated_at = false; break;
        default;
    }
    if (empty($field['decorators'])): ?>
            '<?= $field['property'] ?>',
<?php else: ?>
            <?= "'{$field['property']}' => \$this->{$field['decorators']}" ?>,
<?php endif;
endforeach; ?>
<?=$generate_visible ? "            'visible' => \$this->boolean(),\n": '' ?>
<?=$generate_sort ? "            'sort' => \$this->integer(),\n": '' ?>
<?=$generate_created_at ? "            'created_at' => \$this->integer(),\n": '' ?>
<?=$generate_updated_at ? "            'updated_at' => \$this->integer(),\n": '' ?>
        ]);
<?= $this->render('_addForeignKeys', [
    'table' => $table,
    'foreignKeys' => $foreignKeys,
]);
