<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063402_Relations extends Migration
{

    public function init()
    {
       $this->db = 'db';
       parent::init();
    }

    public function safeUp()
    {
        $this->addForeignKey('fk_menu_item_parent_id',
            '{{%menu_item}}','parent_id',
            '{{%menu_item}}','id',
            'CASCADE','CASCADE'
         );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_menu_item_parent_id', '{{%menu_item}}');
    }
}
