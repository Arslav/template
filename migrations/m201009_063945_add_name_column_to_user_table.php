<?php

use yii\db\Migration;

/**
 * Handles adding name to table `user`.
 */
class m201009_063945_add_name_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'name');
    }
}
