<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063351_contact extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%contact}}',
            [
                'id'=> $this->primaryKey(11),
                'type'=> $this->string(250)->null()->defaultValue(null),
                'name'=> $this->string(250)->null()->defaultValue(null),
                'phone'=> $this->string(250)->null()->defaultValue(null),
                'email'=> $this->string(250)->null()->defaultValue(null),
                'message'=> $this->text()->null()->defaultValue(null),
                'seen'=> $this->boolean()->notNull()->defaultValue(0),
                'created_at'=> $this->integer(10)->unsigned()->notNull()->defaultValue(0),
                'updated_at'=> $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%contact}}');
    }
}
