<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063355_page extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%page}}',
            [
                'id'=> $this->primaryKey(11),
                'title'=> $this->string(255)->notNull(),
                'seo_title'=> $this->string(255)->null()->defaultValue(null),
                'seo_keywords'=> $this->string(255)->null()->defaultValue(null),
                'seo_description'=> $this->string(255)->null()->defaultValue(null),
                'slug'=> $this->string(255)->notNull(),
                'content'=> $this->text()->null()->defaultValue(null),
                'template'=> $this->string(255)->notNull()->defaultValue('pageDefault'),
                'created_at'=> $this->bigInteger(20)->unsigned()->notNull()->defaultValue('0'),
                'updated_at'=> $this->bigInteger(20)->unsigned()->notNull()->defaultValue('0'),
            ],$tableOptions
        );
        $this->createIndex('uk_page_slug','{{%page}}',['slug'],true);

    }

    public function safeDown()
    {
        $this->dropIndex('uk_page_slug', '{{%page}}');
        $this->dropTable('{{%page}}');
    }
}
