<?php

use yii\db\Schema;
use yii\db\Migration;

class m201009_063356_redirect extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%redirect}}',
            [
                'id'=> $this->primaryKey(11),
                'from'=> $this->string(250)->notNull(),
                'to'=> $this->string(250)->notNull(),
                'code'=> $this->integer(11)->notNull(),
                'created_at'=> $this->bigInteger(20)->unsigned()->notNull()->defaultValue('0'),
                'updated_at'=> $this->bigInteger(20)->unsigned()->notNull()->defaultValue('0'),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%redirect}}');
    }
}
