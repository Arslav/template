<?php
use yii\helpers\Html;
use app\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user User */

$resetLink = Url::to(['user/reset-password','token' => $user->password_reset_token], true);

$logoImg = '';
if (Yii::$app->settings->get('SiteSetting', 'logoImg')) {
    $logoImg = Yii::$app->settings->get('SiteSetting', 'logoImg');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title><?= Yii::$app->name; ?></title>
    <!--[if (mso 16)]>
    <style type="text/css">
        a {text-decoration: none;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]>
    <style>sup { font-size: 100% !important; }</style>
    <![endif]-->
</head>
<body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin: 0; font-family: PT Sans, Arial,Arial, Helvetica, sans-serif; padding: 0; width: 100%;">
<div class="es-wrapper-color" style="background-color: transparent;">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="" color="transparent"></v:fill>
    </v:background>
    <![endif]-->
    <table class="es-wrapper" cellspacing="0" cellpadding="0" style="margin: 0; border-collapse: collapse; border-spacing: 0px; height: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; width: 100%;">
        <tbody>
        <tr style="border-collapse: collapse;">
            <td class="esd-email-paddings" valign="top" style="margin: 0; padding: 0;">
                <table class="es-content esd-header-popover" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse; border-spacing: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; table-layout: fixed !important; min-width: 670px; width: 100%;">
                    <tbody>
                    <tr style="border-collapse: collapse;">
                        <td class="esd-stripe esd-checked" align="center" bgcolor="#ecf3ff" style="margin: 0; background-color: #ecf3ff; padding: 10% 0; width: 100%;">
                            <table class="es-content-body" style="min-width: 670px; max-width: 670px; width: 670px;" cellspacing="0" cellpadding="0" align="center" style="background-color: transparent; border-collapse: collapse; border-spacing: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                <tbody>
                                <tr style="border-collapse: collapse;">
                                    <td class="esd-structure es-p30t es-p30b es-p15r es-p15l" align="left" style="margin: 0; padding: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 30px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border-spacing: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                                            <tbody>
                                            <tr style="border-collapse: collapse;">
                                                <td class="esd-container-frame" width="640" valign="top" align="center" style="margin: 0; padding: 0; width: 640px">
                                                    <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border-spacing: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                                                        <tbody>
                                                        <tr style="border-collapse: collapse;">
                                                            <td align="center" class="esd-block-image es-p30b" style="margin: 0; padding: 0; padding-bottom: 30px;"> <img src="<?= Yii::$app->request->hostInfo . $logoImg ?>" alt="" style="-ms-interpolation-mode: bicubic; border: 0; display: block; outline: none; text-decoration: none; height: 40px" height="40"> </td>
                                                        </tr>
                                                        <tr style="border-collapse: collapse;">
                                                            <td align="center" class="esd-block-text" style="margin: 0; padding: 0; padding-bottom: 10px; padding-top: 10px; background-color: #4386fb">
                                                                <p style="-ms-text-size-adjust: none; -webkit-text-size-adjust: none; margin: 0; color: #ffffff; font-family: PT Sans, Arial,Arial, Helvetica, sans-serif; font-size: 18px; line-height: 150%; mso-line-height-rule: exactly;">
                                                                    Здравствуйте <?= Html::encode($user->name) ?> !
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse: collapse;">
                                    <td class="esd-structure es-p20t es-p20b es-p15r es-p15l" align="left" bgcolor="#ffffff" style="margin: 0; background-color: rgb(255,255,255); padding: 0; padding-bottom: 20px; padding-left: 15px; padding-right: 15px; padding-top: 20px; border: 1px solid #dddddd; border-top: none;">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; border-spacing: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%">
                                            <tbody>
                                            <tr style="border-collapse: collapse;">
                                                <td width="640" class="esd-container-frame" align="center" valign="top" style="margin: 0; padding: 0; width: 640px">
                                                    <table cellpadding="0" cellspacing="0" width="640px" style="border-collapse: collapse; border-spacing: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 640px; table-layout: fixed !important;">
                                                        <tbody>
                                                        <tr style="border-collapse: collapse;">
                                                            <td align="center" class="esd-block-text es-p20b" style="margin: 0; padding: 0; padding-bottom: 20px;">
                                                                <p style="-ms-text-size-adjust: none; -webkit-text-size-adjust: none; margin: 0; color: #000000; font-family: PT Sans, Arial,Arial, Helvetica, sans-serif; font-size: 17px; line-height: 150%; mso-line-height-rule: exactly;">
                                                                    <strong>Восстановление пароля!</strong>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr style="border-collapse: collapse;">
                                                            <td align="left" class="esd-block-text es-p20b" style="margin: 0; padding: 0; padding-bottom: 20px;">
                                                                <p style="-ms-text-size-adjust: none; -webkit-text-size-adjust: none; margin: 0; color: #000000; font-family: PT Sans, Arial,Arial, Helvetica, sans-serif; font-size: 16px; line-height: 150%; mso-line-height-rule: exactly;">
                                                                    Для создания нового пароля перейдите по ссылке:
                                                                </p>
                                                                <p>
                                                                    <?= Html::a(Html::encode($resetLink), $resetLink) ?>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="esd-footer-popover_mailru_css_attribute_postfix es-content_mailru_css_attribute_postfix" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;border-spacing: 0px;table-layout: fixed !important;width: 100%;">
                    <tbody>
                    <tr style="border-collapse: collapse;">
                        <td class="esd-stripe_mailru_css_attribute_postfix" align="center" bgcolor="#4386fb" style="margin: 0;background-color: #4386fb;padding: 0;">
                            <table class="es-content-body_mailru_css_attribute_postfix" style="min-width: 670px;max-width: 670px;width: 670px;" cellspacing="0" cellpadding="0" align="center" bgcolor="transparent">
                                <tbody>
                                <tr style="border-collapse: collapse;">
                                    <td class="esd-structure_mailru_css_attribute_postfix es-p30t_mailru_css_attribute_postfix es-p30b_mailru_css_attribute_postfix es-p15l_mailru_css_attribute_postfix" align="left" style="margin: 0;padding: 0;padding-bottom: 30px;padding-left: 15px;padding-right: 15px;padding-top: 30px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border-spacing: 0px;width: 100%">
                                            <tbody>
                                            <tr style="border-collapse: collapse;">
                                                <td class="esd-container-frame_mailru_css_attribute_postfix" width="640" valign="top" align="center" style="margin: 0;padding: 0;">
                                                    <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border-spacing: 0px;width: 640px;">
                                                        <tbody>
                                                        <tr style="border-collapse: collapse;">
                                                            <td align="left" class="esd-block-text_mailru_css_attribute_postfix" style="margin: 0;padding: 0;">
                                                                <p style="-ms-text-size-adjust: none;-webkit-text-size-adjust: none;margin: 0;color: #FFFFFF;font-family: PT Sans, Arial,Arial, Helvetica, sans-serif;font-size: 16px;line-height: 150%;text-align: center;" class="MsoNormal_mailru_css_attribute_postfix">Вы получили это письмо по тому, что являетесь пользователем <a href="<?= Yii::$app->request->hostInfo ?>" target="_blank" style="-ms-text-size-adjust: none;-webkit-text-size-adjust: none;color: #FFFFFF;font-family: PT Sans, Arial,Arial, Helvetica, sans-serif;font-size: 16px;text-decoration: underline;" class="MsoNormal_mailru_css_attribute_postfix" rel=" noopener noreferrer"><?= Yii::$app->request->hostInfo ?></a></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>



