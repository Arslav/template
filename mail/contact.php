<?php

use app\models\Contact;
/* @var $contact Contact */
?>

<?php if($contact->name) : ?>
	<div>
		<strong>Имя:</strong>
		<span><?php echo $contact->name; ?></span>
	</div>
<?php endif; ?>

<?php if($contact->phone) : ?>
	<div>
		<strong>Телефон:</strong>
		<span><?php echo $contact->phone; ?></span>
	</div>
<?php endif; ?>

<?php if($contact->type) : ?>
    <div>
        <strong>Тип:</strong>
        <span><?php echo $contact->type; ?></span>
    </div>
<?php endif; ?>

<?php if($contact->email) : ?>
	<div>
		<strong>Email:</strong>
		<span><?php echo $contact->email; ?></span>
	</div>
<?php endif; ?>

<?php if($contact->message) : ?>
	<div>
		<strong>Сообщение</strong>
		<span><?php echo $contact->message; ?></span>
	</div>
<?php endif; ?>
