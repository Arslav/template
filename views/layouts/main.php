<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\web\View;

/* @var $this View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" id="sibling">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <!-- Favicon -->
    <link rel="icon" type="images/ico" href="/images/icons/fav.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title ?: Yii::$app->name) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="home">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <?= $this->render('/templates/_header') ?>
        <!--content-->
        <main class="content">
            <?= $content ?>
        </main>
    </div>
    <div id="bottom" class="footer">
        <?= $this->render('/templates/_bottom') ?>
    </div>
    <?= $this->render('/templates/_footer') ?>
    <?= $this->render('/templates/_modal') ?>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>