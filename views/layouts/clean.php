<?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <?= Html::csrfMetaTags() ?>
    <!-- Favicon -->
    <link rel="icon" type="image/ico" href="/images/favicon.jpeg">
    <title><?= Html::encode($this->title ? : Yii::$app->name) ?></title>
    <script src="/js/dynamicViewport.js"></script>
    <?php $this->head() ?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="home">
<?php $this->beginBody() ?>

    <div class="wrapper">

        <div class="content">
            <div class="p-y-50 p-md-y-60">
              <div class="clean">
                  <?= $content ?>
              </div>
            </div>
        </div>

    </div>

    <div id="bottom">

    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
