<?php

use app\models\Page;

/* @var $model Page */

$this->title = $model->seo_title ?: $model->title;
if ($model->seo_keywords) $this->registerMetaTag(['name' => 'keywords', 'content' => $model->seo_keywords]);
if ($model->seo_description) $this->registerMetaTag(['name' => 'description', 'content' => $model->seo_description]);

?>

<div class="intro-page">
    <div class="container">
        <div class="breadcrumb-block">
            <ul class="breadcrumb">
                <li class="">
                    <a class="" href="/">Главная</a>
                </li>
                <li class=""><?= $model->title ?>
                </li>
            </ul>
        </div>
        <h1><?= $model->title ?></h1>
    </div>
</div>

<!-- Default page -->
<div class="text-block p-b">
    <div class="container">
        <div class="tb-content m-n-b-15 title-p">
            <?= $model->content ?>
        </div>
    </div>
</div>
