<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        //'layout' => 'horizontal',
        'fieldConfig' => [
//            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
    
    
        <?= $form->field($model, 'email', [
    'template' => '<div class="input-group"><span class="input-group-addon"><i class="ti-user"></i></span>{input}</div>{error}{hint}'
])->textInput(['autofocus' => true, 'class' => 'input-lg form-control']) ?>
    
        <?= $form->field($model, 'password', [
    'template' => '<div class="input-group"><span class="input-group-addon"><i class="ti-key"></i></span>{input}</div>{error}{hint}'
])->passwordInput(['class' => 'input-lg form-control']) ?>

        <?= '';//$form->field($model, 'rememberMe')->checkbox([
//            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
//        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('<i class="ti-unlock"></i>' . Yii::t('app', 'Login'), [
                'class' => 'btn btn-danger btn-block', 'name' => 'login-button'
                ]) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
