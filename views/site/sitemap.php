<?php

/* @var $host string */
/* @var $sitemap array */

echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?= $host ?></loc>
        <lastmod><?= date('Y-m-d') ?></lastmod>
    </url>
    <url>
        <loc><?= $host ?>/service</loc>
        <lastmod><?= date('Y-m-d') ?></lastmod>
    </url>
    <url>
        <loc><?= $host ?>/partner</loc>
        <lastmod><?= date('Y-m-d') ?></lastmod>
    </url>
    <url>
        <loc><?= $host ?>/article</loc>
        <lastmod><?= date('Y-m-d') ?></lastmod>
    </url>
    <?php
    foreach ($sitemap as $type => $items) {
        foreach ($items as $item) {

            $loc = $host . '/' . $type . '/' . $item['slug'];
            $lastmod = date('Y-m-d', $item['updated_at']);

            ?>

            <url>
                <loc><?= $loc ?></loc>
                <lastmod><?= $lastmod ?></lastmod>
            </url>

            <?php
        }
    }
    ?>
</urlset>