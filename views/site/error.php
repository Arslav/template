<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="intro-page">
    <div class="container">
    </div>
</div>

<div class="container">
    <div class="text-block">
        <div class="container">
            <h1 class="tb-title title-404 text-main text-center m-b-20"><?= $exception->statusCode ?></h1>
            <div class="tb-sub-title title-2 text-center m-b-20 ">Что-то пошло не так...</div>
            <div class="tb-content text-center">
                <?= Html::encode($message); ?>
            </div>
            <div class="m-t-30 m-b-60 text-center">
                <a href="/" class="btn btn-default">Перейти на главную</a>
            </div>
        </div>
    </div>
</div>