<!-- Default page -->
<div class="content">
    <div class="intro-page">
        <!-- Default page -->
        <div class="text-block">
            <div class="container">
                <h1 class="tb-title title-1 m-b-30 m-lg-b-60  font-w-bold">Спасибо!</h1>
                <div class="title-6">
                    Сообщение успешно отправлено. Наш менеджер свяжется с Вами в ближайшее время.
                </div>
                <div class="m-t-60">
                    <a href="/" class="btn btn-default">Перейти на главную</a>
                </div>
            </div>
        </div>
    </div>
</div>
