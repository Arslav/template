<?php

use app\models\AboutSlide;
use app\models\Apartment;
use app\models\Coordinates;
use app\models\Document;
use app\models\GalleryItem;
use app\models\Place;
use app\models\PlaceSlide;
use app\models\Slide;
use app\models\Video;
use yii\web\View;

/* @var $this View */
/* @var $slides Slide[] */
/* @var $places Place[] */
/* @var $place_slide PlaceSlide[] */
/* @var $apartments Apartment[] */
/* @var $galleryItems GalleryItem[] */
/* @var $videos Video[] */
/* @var $documents Document[] */
/* @var $about_slides AboutSlide[] */
/* @var $coordinates Coordinates[] */

$this->title = Yii::$app->settings->get('SiteSettings', 'homePageTitle') ?: Yii::$app->name;
if (Yii::$app->settings->get('SiteSettings', 'homePageKeywords')) $this->registerMetaTag(['name' => 'keywords', 'content' => Yii::$app->settings->get('SiteSettings', 'homePageKeywords')]);
if (Yii::$app->settings->get('SiteSettings', 'homePageDescription')) $this->registerMetaTag(['name' => 'description', 'content' => Yii::$app->settings->get('SiteSettings', 'homePageDescription')]);

?>