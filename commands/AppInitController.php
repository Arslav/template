<?php

namespace app\commands;

use app\models\Color;
use app\models\Size;
use yii\console\Controller;
use yii\console\ExitCode;

class AppInitController extends Controller
{
    public function actionIndex()
    {
        $this->createMetrics();
        echo "Done!".PHP_EOL;
        return ExitCode::OK;
    }

    private function createMetrics()
    {
        $path = \Yii::getAlias('@app').'/views/templates/_metrics.php';
        if(file_exists($path)){
            echo "/views/templates/_metrics.php already exist".PHP_EOL;
            return;
        }
        echo "Creating file /views/templates/_metrics.php...".PHP_EOL;
        $fp = fopen($path, "w");
        fwrite($fp, "");
        fclose($fp);
    }
}
