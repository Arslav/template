<?php

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

class CreateUserController extends Controller
{

    public function actionIndex($email = 'admin@admin.ru', $name = 'Admin', $password = '1234')
    {
        $user = new User();
        $user->detachBehavior('saveFile');
        $user->name = $name;
        $user->email = $email;
        $user->status = User::STATUS_ACTIVE;
        $user->role = User::ROLE_ADMIN;
        $user->setPassword($password);
        $user->auth_key = '';
        $user->save();
        echo "User $user->email created!".PHP_EOL;
        return ExitCode::OK;
    }
}
