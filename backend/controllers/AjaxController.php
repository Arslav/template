<?php

namespace app\backend\controllers;

use app\models\HouseProjectImage;
use app\models\ProjectImage;
use Yii;
use app\backend\components\AdminController;

/**
 * Default controller for the `backend` module
 */
class AjaxController extends AdminController
{
    
public function actions()
{
    return [
        'image-upload' => [
            'class' => 'vova07\imperavi\actions\UploadFileAction',
            'url' => \yii\helpers\Url::home(true) . 'images/editor', // Directory URL address, where files are stored.
            'path' => '@webroot/images/editor', // Or absolute path to directory where files are stored.
            'uploadOnlyImage' => true, // For any kind of files uploading.
            'unique' => true,
            'replace' => false, // By default it throw an excepiton instead.
            'translit' => true,//translit its name
            
        ],
    ];
}
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionDeleteImage()
    {
        $model = 'app\\models\\'.Yii::$app->request->get('model');
        $field = Yii::$app->request->get('field');
        $file = Yii::$app->request->post('key');
        if ($file) {
            $path = Yii::getAlias('@webroot' . $file);
            if (is_file($path)) {
                unlink($path);
                $elem = $model::findOne([$field => $file]);
                if ($elem) $elem->{$field} = null;
                $elem->updateAttributes([$field]);
            }
            return true;
        }
        return false;
    }
    
    public function actionDeleteSiteFilesImage()
    {
        $model = 'app\\models\\'.Yii::$app->request->get('model');
        $field = Yii::$app->request->get('field');
        $file = Yii::$app->request->post('key');
        //var_dump($field); var_dump($file); die();
        if ($file) {
            $path = Yii::getAlias('@webroot' . $file);
            if (is_file($path)) {
                unlink($path);
                 Yii::$app->settings->remove('SitePictures', $field);    
            }
            return true;
        }
        return false;
    }

    /**
     * Сортировка изображений для HouseProject
     * TODO: Сделать универсальное решение!
     * @param $id
     * @return bool
     */
    public function actionSortHouseProjectImage($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = \app\models\HouseProject::findOne(['id' => $id]);
            $post = Yii::$app->request->post('sort');
            $post['newIndex']++;
            $post['oldIndex']++;
            if ($post['oldIndex'] > $post['newIndex']) {
                $param = ['and', ['>=', 'sort', $post['newIndex']], ['<', 'sort', $post['oldIndex']]];
                $counter = 1;
            } else {
                $param = ['and', ['<=', 'sort', $post['newIndex']], ['>', 'sort', $post['oldIndex']]];
                $counter = -1;
            }
            HouseProjectImage::updateAllCounters(['sort' => $counter], [
                'and', ['house_project_id' => $id], $param
            ]);
            HouseProjectImage::updateAll(['sort' => $post['newIndex']], [
                'id' => $post['stack'][--$post['newIndex']]['key']
            ]);
            return true;
        }
        return false;
    }

    /**
     * Сортировка изображений для HouseProject
     * TODO: Сделать универсальное решение!
     * @param $id
     * @return bool
     */
    public function actionSortProjectImage($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = \app\models\Project::findOne(['id' => $id]);
            $post = Yii::$app->request->post('sort');
            $post['newIndex']++;
            $post['oldIndex']++;
            if ($post['oldIndex'] > $post['newIndex']) {
                $param = ['and', ['>=', 'sort', $post['newIndex']], ['<', 'sort', $post['oldIndex']]];
                $counter = 1;
            } else {
                $param = ['and', ['<=', 'sort', $post['newIndex']], ['>', 'sort', $post['oldIndex']]];
                $counter = -1;
            }
            ProjectImage::updateAllCounters(['sort' => $counter], [
                'and', ['project_id' => $id], $param
            ]);
            ProjectImage::updateAll(['sort' => $post['newIndex']], [
                'id' => $post['stack'][--$post['newIndex']]['key']
            ]);
            return true;
        }
        return false;
    }

    /**
     * Delete file from setting model
     * @return string
     */
    public function actionDeleteFileSetting()
    {
        $model = Yii::$app->request->get('model');
        $field = Yii::$app->request->get('field');
        $file = Yii::$app->request->post('key');
        if ($file) {
            $path = Yii::getAlias('@webroot' . $file);
            if (is_file($path)) {
                unlink($path);
                Yii::$app->settings->remove($model, $field);
            }
            return true;
        }
        return false;
    }

    /**
     * Delete related object from parent form
     * @return string
     */
    public function actionDeleteObject()
    {
        $model = 'app\\models\\'.Yii::$app->request->get('model');
        $key = Yii::$app->request->post('key');
        $elem = $model::findOne(['id' => $key]);
        if ($elem) $elem->delete();
        return true;
    }

}
