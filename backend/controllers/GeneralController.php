<?php

namespace app\backend\controllers;

use app\backend\components\AdminController;
use Yii;


class GeneralController extends AdminController
{
    
function actions(){
        return [
            'index' => [
                'class' => \yii2mod\settings\actions\SettingsAction::class,
                'modelClass' => \app\models\SiteInfo::class,
                'view' => 'index',
                'successMessage' => 'Информация успешно обновлена',
                'saveSettings' => function($model) {
                    $model->saveFile();
                    foreach ($model->toArray() as $key => $value) {
                        if ($value === '')
                            Yii::$app->settings->remove('SiteInfo', $key);
                        else {
                            Yii::$app->settings->set('SiteInfo', $key, $value);
                        }
                    }    
                }
            ],
        ];
}
    

}
