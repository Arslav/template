<?php

namespace app\backend\controllers;

use app\backend\components\AdminController;
use \yii\filters\AccessControl;
use app\backend\components\rbac\AccessRule;
use app\models\User;
/**
 * Default controller for the `backend` module
 */
class DefaultController extends AdminController
{
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
         return $this->redirect('/admin-login');
        //return $this->render('index');
    }
}
