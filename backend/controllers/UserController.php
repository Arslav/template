<?php

namespace app\backend\controllers;

use app\models\Payment;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\backend\components\AdminController; 
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\backend\components\rbac\AccessRule;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AdminController
{

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $exportDataProvider = clone $dataProvider;
        $exportDataProvider->pagination->pageSize = 0;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'exportDataProvider' => $exportDataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($_POST['apply']))
                return $this->redirect(['update', 'id' => $model->id]);
            elseif(isset($_POST['new']))
                return $this->redirect(['create']);
            else
                return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($_POST['apply']))
                return $this->redirect(['update', 'id' => $model->id]);
            elseif(isset($_POST['new']))
                return $this->redirect(['create']);
            else
                return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates identity
     */
    public function actionUpdateProfile()
    {
        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Информация успешно обновлена');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Updates password
     */
    public function actionUpdatePassword()
    {
        $user = Yii::$app->user->identity;
        $model = new User();
        $model->scenario = User::SCENARIO_PASSWORD;
        if ($model->load(Yii::$app->request->post())) {
            if ($user->validatePassword($model->password_old)) {
                if ($user->load(Yii::$app->request->post()) && $user->save()) {
                    Yii::$app->session->setFlash('success', 'Пароль изменен успешно');
                    return $this->refresh();
                }
            } else {
                Yii::$app->session->setFlash('warning' , 'Старый пароль неверный');
            }
        }

        return $this->render('updatePassword', [
            'model' => $user,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $childDataProvider = new ActiveDataProvider([
            'query' => $model->getChildrenQuery(),
        ]);
        $paymentDataProvider = new ActiveDataProvider([
            'query' => Payment::find()->where(['user_id' => $model->id]),
        ]);
        return $this->render('view', [
            'model' => $model,
            'child' => $childDataProvider,
            'payment' => $paymentDataProvider,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteUser($id)
    {
        $user = $this->findModel($id);
        $user->email = $user->email.'_deleted';
        $user->save();
        $user->delete();
        return $this->redirect(Yii::$app->request->referrer ? : '/admin');
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
