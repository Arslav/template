<?php

use app\models\GeneralModel;
use app\models\SiteInfo;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use vova07\imperavi\Widget;
use kartik\file\FileInput;
use dosamigos\fileupload\FileUpload;

/* @var $model SiteInfo */
?>

<?php
$this->title = 'Информация на сайте';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="m-b-15">
    <h3 class="font-weight-bold">Информация на сайте</h3>
</div>

<div class="form">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'site-settings-form',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-2',
                'wrapper' => 'col-lg-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">

        <div class="col-md-12">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="first">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Блок "О проекте"
                        </div>
                        <div class="panel-body">
                            <?=$form->field($model,'aboutProjectName')->textInput() ?>
                            <?=$form->field($model,'aboutProjectTitle')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 100,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?=$form->field($model,'aboutProjectImage1')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=aboutProjectImage1']),
                                    'initialPreview' => ($model->aboutProjectImage1) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->aboutProjectImage1]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?=$form->field($model,'aboutProjectImage2')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=aboutProjectImage2']),
                                    'initialPreview' => ($model->aboutProjectImage2) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->aboutProjectImage2]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?=$form->field($model,'aboutProjectImage3')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=aboutProjectImage3']),
                                    'initialPreview' => ($model->aboutProjectImage3) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->aboutProjectImage3]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?=$form->field($model,'aboutProjectContent')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 200,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">Блок "Изображения в сладере"</div>
                        <div class="panel-body">
                            <?= $form->field($model,'imageAboutImage1')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=imageAboutImage1']),
                                    'initialPreview' => ($model->imageAboutImage1) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->imageAboutImage1]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'imageAboutImage2')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=imageAboutImage2']),
                                    'initialPreview' => ($model->imageAboutImage2) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->imageAboutImage2]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'imageAboutImage3')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=imageAboutImage3']),
                                    'initialPreview' => ($model->imageAboutImage3) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->imageAboutImage3]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'imageAboutImage4')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=imageAboutImage4']),
                                    'initialPreview' => ($model->imageAboutImage4) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->imageAboutImage4]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'imageAboutImage5')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=imageAboutImage5']),
                                    'initialPreview' => ($model->imageAboutImage5) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->imageAboutImage5]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'imageAboutImage6')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=imageAboutImage6']),
                                    'initialPreview' => ($model->imageAboutImage6) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->imageAboutImage6]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Блок "Расположение"
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model,'locationName')->textInput() ?>
                            <?= $form->field($model,'locationTitle')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 100,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?= $form->field($model,'locationText')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 200,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?= $form->field($model,'locationImage')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=locationImage']),
                                    'initialPreview' => ($model->locationImage) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->locationImage]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Блок "Преимущества"
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model,'advantageName')->textInput() ?>
                            <?= $form->field($model,'advantageTitle')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 100,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?= $form->field($model,'advantageImage1')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=advantageImage1']),
                                    'initialPreview' => ($model->advantageImage1) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->advantageImage1]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'advantageTitle1')->textInput() ?>
                            <?= $form->field($model,'advantageText1')->textInput() ?>
                            <?= $form->field($model,'advantageImage2')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=advantageImage2']),
                                    'initialPreview' => ($model->advantageImage2) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->advantageImage2]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'advantageTitle2')->textInput() ?>
                            <?= $form->field($model,'advantageText2')->textInput() ?>
                            <?= $form->field($model,'advantageImage3')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=advantageImage3']),
                                    'initialPreview' => ($model->advantageImage3) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->advantageImage3]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'advantageTitle3')->textInput() ?>
                            <?= $form->field($model,'advantageText3')->textInput() ?>
                            <?= $form->field($model,'advantageImage4')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=advantageImage4']),
                                    'initialPreview' => ($model->advantageImage4) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->advantageImage4]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'advantageTitle4')->textInput() ?>
                            <?= $form->field($model,'advantageText4')->textInput() ?>
                            <?= $form->field($model,'advantageImage5')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showUpload' => false,
                                    'showClose' => false,
                                    'imageUpload' => \yii\helpers\Url::to(['/admin/ajax/image-upload']),
                                    'deleteUrl' => \yii\helpers\Url::toRoute(['/admin/ajax/delete-file-setting?model=SiteInfo&field=advantageImage5']),
                                    'initialPreview' => ($model->advantageImage5) ? : false,
                                    'initialPreviewConfig' => [['key' => $model->advantageImage5]],
                                    'initialPreviewAsData' => true,
                                ],

                            ]); ?>
                            <?= $form->field($model,'advantageTitle5')->textInput() ?>
                            <?= $form->field($model,'advantageText5')->textInput() ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Блок "Планировки"
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model,'apartmentsName')->textInput() ?>
                            <?= $form->field($model,'apartmentsTitle')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 100,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Блок "Ход строительства"
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model,'videoName')->textInput() ?>
                            <?= $form->field($model,'videoTitle')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 100,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?= $form->field($model,'videoTranslationLink')->textInput() ?>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Блок "Наша деятельность"
                        </div>
                        <div class="panel-body">
                            <?= $form->field($model,'aboutName')->textInput() ?>
                            <?= $form->field($model,'aboutTitle')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 100,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?= $form->field($model,'aboutContent')->widget(vova07\imperavi\Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 200,
                                        'plugins' => [
                                            'counter',
                                            'table',
                                            'video',
                                            'fontsize',
                                            'fontcolor',
                                            'fontfamily',
                                            'fullscreen',
                                        ],
                                    ],
                                ]); ?>
                            <?= $form->field($model,'aboutSberText')->textInput() ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS
function videoRender() {
    let video_url = $('.video').val();
    let html = '';
    if (video_url) {
        html += '<iframe src="http://www.youtube.com/embed/' + video_url.replace('https://www.youtube.com/watch?v=', '').replace('https://youtu.be/', '') + '" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>';
    }
    $('.video_preview').html(html);
}
videoRender();
$('.video').change(videoRender);
JS;

$this->registerJs($script);
?>