<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserLog;

/* @var $this yii\web\View */
/* @var $model app\models\search\UserLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>


    <?= $form->field($model, 'type')->dropDownList(UserLog::getTypeOptions(), ['placeholder' => Yii::t('app', 'Type'), 'prompt' => 'Выберите тип']) ?>

    <?= $form->field($model, 'table')->textInput(['placeholder' => Yii::t('app', 'Table')]) ?>

    <?= $form->field($model, 'column')->textInput(['placeholder' => Yii::t('app', 'Column')]) ?>

    <div class="form-group">
        <?= Html::submitButton('<span class="btn-label"><i class="ti-search"></i></span>' . Yii::t('app', 'Search'), ['class' => 'label-left btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Reset'), 'index', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
