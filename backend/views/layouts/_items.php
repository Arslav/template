<?php
$controllerId = Yii::$app->controller->id;
$method = Yii::$app->controller->action->id;

return [
    ['label' => Yii::t('app', 'Contacts'), 'url'=>'/admin/contact', 'active' => ($controllerId == 'contact')],
    ['label' => Yii::t('app', 'Blocks'),
        'items' => [
            ['label' => Yii::t('app', 'Site Info'), 'url' => '/admin/general', 'active' => ($controllerId == 'general')],
            ['label' => Yii::t('app', 'Slides'), 'url' => '/admin/slide', 'active' => ($controllerId == 'slide')],
            ['label' => Yii::t('app', 'Promo Blocks'), 'url' => '/admin/promo-block', 'active' => ($controllerId == 'promo-block')],

        ],
        'active' => in_array($controllerId, []),
        'options' => ['class'=>'with-sub'],
        'template' => '<span>{label}</span>',
    ],
    ['label' => Yii::t('app', 'Catalog'),
        'items' => [
            ['label' => Yii::t('app', 'Categories'), 'url' => '/admin/category', 'active' => ($controllerId == 'category')],
            ['label' => Yii::t('app', 'Products'), 'url' => '/admin/product', 'active' => ($controllerId == 'product')],
        ],
        'active' => in_array($controllerId, []),
        'options' => ['class'=>'with-sub'],
        'template' => '<span>{label}</span>',
    ],
    ['label' => Yii::t('app','Materials'),
        'items' => [
            ['label' => Yii::t('app', 'Sizes'), 'url' => '/admin/size', 'active' => ($controllerId == 'size')],
            ['label' => Yii::t('app', 'Colors'), 'url' => '/admin/color', 'active' => ($controllerId == 'color')],
        ],
        'active' => in_array($controllerId, []),
        'options' => ['class'=>'with-sub'],
        'template' => '<span>{label}</span>',
    ],
    ['label' => Yii::t('app', 'Pages'), 'url' => '/admin/page', 'active' => ($controllerId == 'page')],
    ['label' => Yii::t('app', 'Menu'), 'url' => '/admin/menu', 'active' => ($controllerId == 'menu')],
];