<?php

use app\models\SiteSettings;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/* @var $model SiteSettings */
?>



<?php
$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-b-15">
    <h3 class="font-weight-bold">
        <?php echo Yii::t('app', 'Settings'); ?>
    </h3>
</div>

<div class="form">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'id' => 'site-settings-form',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-3',
                'wrapper' => 'col-lg-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="#first" data-toggle="tab">Главная</a></li>
                <li><a href="#third" data-toggle="tab">SEO</a></li>
                <li><a href="#fourth" data-toggle="tab">SMTP</a></li>
            </ul>
        </div>
        <div class="col-md-10">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="first">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Контактные данные
                        </div>
                        <div class="panel-body">
                            <?php
                            echo $form->field($model, 'email');
                            echo $form->field($model, 'address');
                            echo $form->field($model, 'workTime');
                            echo $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '+7 (999) 999-99-99',
                                'options' => [
                                    'placeholder' => 'Телефон',
                                    'class' => 'col-xs-12',
                                ]
                            ]);


                            echo $form->field($model, 'centerMap')->textInput();

                            echo Html::a('Добавить/удалить метки', ['/admin/coordinates'], ['class' => 'btn btn-success center-block m-b-20']);

                            ?>
                            <div class="form-group field-sitesetting-contactcoords">
                                <div class="col-lg-9 col-lg-offset-3">
                                    Узнать координаты можно с помощью сервиса <a target="_blank"
                                                                                 href="https://yandex.ru/map-constructor/location-tool/">Яндекс.Геокодер</a>
                                    <br><br>
                                    Порядок действий:
                                    <br>
                                    Найдите нужное здание, используя панель поиска в верхнем левом углу;<br>
                                    Скопируйте координаты из ячейки "Центр" в правом нижнем углу.<br>
                                    Пример: -50.053922594637484, 43.227183161941795
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Ссылка на страницу 'Политики конфиденциальности'
                        </div>
                        <div class="panel-body">
                            <?php
                            echo $form->field($model, 'privacyLink')->textInput()->hint('Относительный путь, начинающийся со знака "/". Например - "/page/name"');
                            ?>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Почта
                        </div>
                        <div class="panel-body">
                            <?php
                            echo $form->field($model, 'sendEmailsTo')->textInput()->hint('Через запятую (без пробела), если email адресов несколько');
                            ?>
                        </div>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="third">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Главная страница
                        </div>
                        <div class="panel-body">
                            <?php
                            echo $form->field($model, 'homePageTitle');
                            echo $form->field($model, 'homePageKeywords');
                            echo $form->field($model, 'homePageDescription');
                           /* echo $form->field($model, 'homePageH1AfterArticlesInfo');
                            echo $form->field($model, 'homePageAfterArticlesInfo')->widget(vova07\imperavi\Widget::className(), [
                                'settings' => [
                                    'lang' => 'ru',
                                    'minHeight' => 200,
                                    'plugins' => [
                                        'counter',
                                        'table',
                                        'video',
                                        'fontsize',
                                        'fontcolor',
                                        'fontfamily',
                                        'fullscreen',
                                    ],
                                ],

                            ]);*/
                            ?>
                        </div>
                    </div>
                </div>


                <div role="tabpanel" class="tab-pane" id="fourth">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            Отправка почты
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-9 col-lg-offset-3">
                                    <div class="alert alert-danger">
                                        Любые изменения могут повлечь сбой при отправке сообщений через обратную связь!
                                    </div>
                                </div>
                            </div>
                            <?php

                            echo $form->field($model, 'smtpHost');
                            echo $form->field($model, 'smtpPort')->textInput([
                                'type' => 'number'
                            ]);
                            //                                    echo $form->field($model, 'smtpAuth')->dropDownList([
                            //                                                            '1' => Yii::t('app', 'Yes'),
                            //                                                            '0' => Yii::t('app', 'No')
                            //                                                       ]);
                            echo $form->field($model, 'smtpUsername');
                            echo $form->field($model, 'smtpPassword')->passwordInput();
                            echo $form->field($model, 'smtpSecure')->dropDownList([
                                'ssl' => 'SSL',
                                'tls' => 'TLS'
                            ]);
                            echo $form->field($model, 'fromEmail');

                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>