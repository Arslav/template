<?php

namespace app\backend\components;

use Yii;
//use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Redirect;
//use yii\web\Response;
//use yii\filters\VerbFilter;
//use app\models\LoginForm;
//use app\models\ContactForm;

class FrontController extends Controller {

    function beforeAction($action) {
        
        /* Redirect */
        $path = trim(Yii::$app->request->getPathInfo(), '/');
        foreach (Redirect::find()->all() as $redirect) {
            if (trim($redirect->from, '/') == $path) {
                return $this->redirect($redirect->to, $redirect->code)->send();
            }
        }
        
        return parent::beforeAction($action);
    }
    
}