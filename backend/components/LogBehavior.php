<?php

namespace app\backend\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class LogBehavior extends Behavior
{

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'addLogInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'addLogUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'addLogDelete',
        ];
    }

    public function addLogInsert( $event )
    {
            $log = new \app\models\UserLog();
//            $log->user_id = \Yii::$app->user->getId();
            $log->user_id = \Yii::$app->user->identity->id;
            $log->created_at = time();
            $log->type = 'insert';
            $log->record_id = $this->owner->id;
            $log->table = $this->owner->formName();
//            var_dump($log);
            $log->save();

    }

    public function addLogUpdate( $event )
    {
            
            foreach($this->owner->attributes() as $attr) {
                if ($this->owner->isAttributeChanged($attr, FALSE)) {
                    $log = new \app\models\UserLog();
                    $log->user_id = \Yii::$app->user->identity->id;
                    $log->created_at = time();
                    $log->type = 'update';
                    $log->record_id = $this->owner->id;
                    $log->table = $this->owner->formName();
                    $log->column = $attr;
                    $log->old_value = $this->owner->getOldAttribute($attr);
                    $log->new_value = $this->owner->getAttribute($attr);
                    //var_dump($log);die;
                    $log->save();
                }
            };

    }

    public function addLogDelete( $event )
    {
            $log = new \app\models\UserLog();
            $log->user_id = \Yii::$app->user->identity->id;
            $log->created_at = time();
            $log->type = 'delete';
            $log->record_id = $this->owner->id;
            $log->table = $this->owner->formName();
            $log->save();

        }
        
}