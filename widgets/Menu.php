<?php
namespace app\widgets;

use Yii;
use app\models\MenuItem;
/**
 * Кастомный кастыльный виджет, который генерирует элементы <li> меню
 */
class Menu extends \yii\bootstrap\Widget
{

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $res = '';
        $menuElems = MenuItem::getMenu(false);
        
        foreach($menuElems as $elem) {
            if ($elem['items']) {
                $res .= '<li class="dropdown' . ($elem['active'] ? ' active' : '') . '">';
                $res .= '<a href="' . $elem['url'] . '">' . $elem['label'] . '<span class="caret"></span></a>';
                $res .= '<a class="dropdown-toggle"></a>';
                $res .= '<ul class="dropdown-menu">';
                foreach ($elem['items'] as $subElem) {
                    $res .= '<li' . ($subElem['active'] ? ' class"active"' : '') . '><a href="' . $subElem['url'] . '">' . $subElem['label'] . '</a></li>';
                }
                $res .= '</ul></li>';
            }else {
                $res .= '<li' . ($elem['active'] ? ' class="active"' : '') . '><a href="' . $elem['url'] . '">' . $elem['label'] . '</a></li>';
            }
        }
        return $res;
    }
}
