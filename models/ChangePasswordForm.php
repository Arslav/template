<?php


namespace app\models;

use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $new_password_repeat;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['old_password','new_password','new_password_repeat'],'string'],
            [['old_password','new_password','new_password_repeat'],'required'],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Пароли не совпадают'],
            ['new_password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('app','Old Password'),
            'new_password' => Yii::t('app','New Password'),
            'new_password_repeat' => Yii::t('app','Repeat Password'),
        ];
    }

    public function changePassword()
    {
        $identify = Yii::$app->user->identity;
        $user = User::findOne(['id' => $identify->getId()]);
        if(!$user->validatePassword($this->old_password))
        {
            $this->addError('old_password','Неверный пароль');
            return false;
        }
        if($this->validate()){
            $user->setPassword($this->new_password);
            $user->save();
            return true;
        }
        return false;
    }


}