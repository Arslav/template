<?php

namespace app\models;

use app\backend\components\FileSaveBehavior;
use app\backend\components\LogBehavior;
use himiklab\thumbnail\EasyThumbnailImage;
use kotchuprik\sortable\behaviors\Sortable;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use yii2tech\ar\softdelete\SoftDeleteQueryBehavior;

/**
 * General class
 * @property string $createdDate
 * @property string $updatedDate
 *
 * @property string $imagePath
 */
class GeneralModel extends ActiveRecord
{
    const HIDDEN = 0;
    const VISIBLE = 1;

    const DELETED = -1;
    const UNDELETED = 0;

    public static $thumbnail_width = 400;
    public static $thumbnail_height = null;
    public static $thumbnail_placeholder_path = '/no-image.png';

    private static $_has_deleted = null;
    private static $_behaviors = null;

    /**
     * Возвращает список поведений в зависимости от существующих полей в таблице:
     * 1. Для полей 'updated_at', 'created_at': TimestampBehavior
     * 2. Для поля 'sort': Sortable
     * 3. Для поля 'slug': SlugBehavior
     * 4. Для полей 'image','thumbnail','file','thumbnailmobile': FileSaveBehavior
     * 5. Для поля 'deleted': 'softDeleteBehavior'
     *
     * @see TimestampBehavior
     * @see Sortable
     * @see SlugBehavior
     * @see FileSaveBehavior
     * @see SoftDeleteBehavior
     *
     * @return array
     * @author Ардесов Вячеслав
     * @version 2.6
     */
    public function behaviors()
    {
        $behaviors = [];
        if (!isset(self::$_behaviors[static::className()])) {
            if (Yii::$app->db->getTableSchema(self::tableName(), true) !== null) {
                if ($this->hasAttribute('sort')) {
                    $behaviors['sortable'] = [
                        'class' => Sortable::className(),
                        'query' => self::find(),
                        'orderAttribute' => 'sort',
                    ];
                }
                if ($this->hasAttribute('updated_at') && $this->hasAttribute('created_at')) {
                    $behaviors[] = TimestampBehavior::className();
                }
                if ($this->hasAttribute('slug')) {
                    $behaviors['slug'] = [
                        'class' => 'app\backend\components\SlugBehavior',
                    ];
                }
                if ($this->hasAttribute('deleted')) {
                    $behaviors['softDeleteBehavior'] = [
                        'class' => SoftDeleteBehavior::className(),
                        'softDeleteAttributeValues' => [
                            'deleted' => true,
                        ],
                        'replaceRegularDelete' => true,
                    ];
                }
                $fields = [];
                if ($this->hasAttribute('thumbnail')) $fields[] = 'thumbnail';
                if ($this->hasAttribute('image')) $fields[] = 'image';
                if ($this->hasAttribute('file')) $fields[] = 'file';
                if ($this->hasAttribute('thumbnailmobile')) $fields[] = 'thumbnailmobile';
                if ($fields) {
                    $behaviors['saveFile'] = [
                        'class' => '\app\backend\components\FileSaveBehavior',
                        'fields' => $fields,
                    ];
                }
            }
            self::$_behaviors[static::className()] = $behaviors;
        }


        return  self::$_behaviors[static::className()];

    }

    /**
     * Метод, возвращающий список объектов массивом вида ключ - значение
     * return array
     */
    public static function getList($id = 'id', $title = 'title') {
        //$all = self::findAll(['deleted' => self::UNDELETED]);
        $all = self::find()->all();
        return yii\helpers\BaseArrayHelper::map($all, $id, $title);
    }

    /**
     * Поиск объекта по ссылке
     */
    public static function findBySlug($slug) {
        $model = self::findOne(['slug' => $slug]);
        if ($model) {
            return $model;
        }else {
            throw new HttpException(404, 'Страница не найдена');
        }
    }

    public static function getVisibleOptions() {
        return [
            self::VISIBLE => 'Видимый',
            self::HIDDEN => 'Скрытый',
        ];
    }

    public static function getDeleteOptions() {
        return [
            self::UNDELETED => 'Активный',
            self::DELETED => 'Удаленный',
        ];
    }

    /**
     * Метод возвращающий список объектов, сортирующий и учитывающий
     * их видимость, при наличии соответствующих полей в базе: 'sort', 'visible'
     *
     * @author Ардесов Вячеслав
     * @version 1.0
     * @return array|ActiveRecord[]
     */
    public static function getAll()
    {
        $query = self::find();
        $columns = Yii::$app->db->getTableSchema(self::tableName())->columns;
        if(isset($columns['visible'])) $query = $query->where(['visible' => true]);
        if(isset($columns['sort'])) $query = $query->orderBy('sort');
        return $query->all();
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function getCreatedDate(){
        return Yii::$app->formatter->asDate($this->created_at);
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function getUpdatedDate(){
        return Yii::$app->formatter->asDate($this->updated_at);
    }

    /**
     * Возвращает изобращение или его плейсхолдер /no-image.png
     * @return string
     * @version 2.0
     * @author Ардесов Вячеслав
     */
    public function getImagePath()
    {
        try
        {
            $file = Yii::getAlias('@webroot').$this->image;
            $stopList = ['svg'];
            if (!file_exists($file)) {
                $file = Yii::getAlias('@webroot') . static::$thumbnail_placeholder_path;
            } elseif (in_array(pathinfo($file, PATHINFO_EXTENSION), $stopList)) {
                return $this->image;
            }
            return EasyThumbnailImage::thumbnailFileUrl($file, static::$thumbnail_width, static::$thumbnail_height, EasyThumbnailImage::THUMBNAIL_INSET, 80);
        }
        catch (\Exception $e)
        {
            return static::$thumbnail_placeholder_path;
        }
    }


    /**
     * Склонение существительных после числительных.
     *
     * @param string $value Значение
     * @param array $words Массив вариантов, например: array('товар', 'товара', 'товаров')
     * @param bool $show Включает значение $value в результирующею строку
     * @return string
     */
    public static function num_word($value, $words, $show = true)
    {
        $num = $value % 100;
        if ($num > 19) {
            $num = $num % 10;
        }

        $out = ($show) ?  $value . ' ' : '';
        switch ($num) {
            case 1:
                $out .= $words[0];
                break;
            case 2:
            case 3:
            case 4:
                $out .= $words[1];
                break;
            default:
                $out .= $words[2];
                break;
        }

        return $out;
    }


    public static function priceformat($price)
    {
        return number_format($price, 0, '.', ' ');
    }

    //Перегрузка с проверкой поля deleted
    public static function find()
    {
        $query = parent::find();
        $_has_delete = false;
        if(!isset(self::$_has_deleted[static::className()])) {
            self::$_has_deleted[static::className()] = Yii::$app->db->getTableSchema(static::tableName())->getColumn('deleted') == true;
        }
        $_has_delete = self::$_has_deleted[static::className()];
        if ($_has_delete) {
            $query->attachBehavior('softDelete', [
                'class' => SoftDeleteQueryBehavior::className(),
                'deletedCondition' => [
                    'deleted' => true,
                ],
            ]);
            $query = $query->notDeleted();
        }
        return $query;
    }

}