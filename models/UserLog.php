<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_log}}".
 *
 * @property int $id
 * @property string $type
 * @property int $record_id
 * @property string $table
 * @property string $column
 * @property string $old_value
 * @property string $new_value
 * @property int $user_id
 * @property int $created_at
 */
class UserLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id', 'user_id', 'created_at'], 'integer'],
            [['user_id'], 'required'],
            [['type', 'table', 'column', 'old_value', 'new_value'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'record_id' => Yii::t('app', 'Record'),
            'table' => Yii::t('app', 'Table'),
            'column' => Yii::t('app', 'Column'),
            'old_value' => Yii::t('app', 'Old Value'),
            'new_value' => Yii::t('app', 'New Value'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
    
    /**
     * Возвращает типы логов
     */
    public static function getTypeOptions()
    {
        return [
            'insert' => 'insert',
            'update' => 'update',
            'delete' => 'delete',
        ];
    }
}
