<?php

namespace app\models;

use Yii;
use DirectoryIterator;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property int $id
 * @property string $title
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $slug
 * @property string $content
 * @property string $content_second
 * @property string $template
 * @property string $created_at
 * @property string $updated_at
 */
class Page extends GeneralModel
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            //[['content_second'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title', 'seo_title', 'seo_keywords', 'seo_description', 'slug'], 'string', 'max' => 255],
            [['template'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'slug' => Yii::t('app', 'Slug'),
            'content' => Yii::t('app', 'Content'),
            //'content_second' => Yii::t('app', 'Content second'),
            'template' => Yii::t('app', 'Template'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    // search files matching template {$prefix}{$name}.php in application.views.templates
    public static function getTemplates($prefix = 'page') {
            $result = array();
            $directory = Yii::getAlias('@app/views/page');

            if(is_dir($directory)) {
                    $iterator = new DirectoryIterator($directory);
                    foreach($iterator as $file) {
                            $matches = array();
                            if($file->isFile() && preg_match('/(' . $prefix . '(.*))\.php/', $file->getFilename(), $matches)) {
                                    // filename => template name
                                    $result[$matches[1]] = Yii::t('app', $matches[1]);
                            }
                    }
            }
            return $result;
    }
    
	public static function getSlugs() {
            $all = self::find()->all();
            return yii\helpers\BaseArrayHelper::map($all, 'slug', 'title');
	}
    
}
