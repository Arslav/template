<?php

namespace app\models;

use YandexCheckout\Client;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password
 * @property string $name [varchar(255)]
 * @property string $image [varchar(255)]
 * @property string $validation_token [varchar(255)]
 * @property string $invite_token [varchar(255)]
 */
class User extends GeneralModel implements IdentityInterface
{
    public $password_old;
    public $password_repeat;
    public $delete_child_id;
    
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const ROLE_USER = 10;
    const ROLE_MODERATOR = 20;
    const ROLE_ADMIN = 30;
    
    const SCENARIO_PASSWORD = 'updatePassword';

    const PAY_STATUS_FREE = null;
    const PAY_STATUS_PAYED = 'payed';
    const PAY_STATUS_EXPIRED = 'expired';

    public static $thumbnail_placeholder_path = '/parent-no-image.jpg';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password_old'], 'string', 'max' => 250],
            ['email', 'email'],
            ['email', 'unique'],
            //[['username'], 'required'],
            //[['username'], 'string', 'max' => 250, 'on' => self::SCENARIO_PASSWORD],
            [['password', 'password_repeat'], 'required', 'on' => self::SCENARIO_PASSWORD],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t('app', 'Passwords dont match')],
            ['password_hash', 'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['delete_child_id'], 'integer'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'password' => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Repeat password'),
            'password_old' => Yii::t('app', 'Old password'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'role' => Yii::t('app', 'Role'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

//    /**
//     * Finds user by username
//     *
//     * @param string $username
//     * @return static|null
//     */
//    public static function findByUsername($username)
//    {
//        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
//    }


    /**
     * @param $email
     * @return User|null
     */
    public static function findByEmail($email){
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    public function getPassword()
    {
        return $this->password_repeat;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Возвращает массив статусов
     */
    public static function getStatusOptions()
    {
        return [
            self::STATUS_DELETED => 'Не активен',
            self::STATUS_ACTIVE => 'Активен',
        ];
    }

    /**
     * Возвращает массив ролей
     */
    public static function getRoleOptions()
    {
        return [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_MODERATOR => 'Модератор',
            self::ROLE_ADMIN => 'Администратор',
        ];
    }

    /**
     * Возвращает название статуса
     */
    public function getStatusName()
    {
        if ($this->status == self::STATUS_ACTIVE) {
            $status = 'Активен';
        } else {
            $status = 'Не активен';
        }
        return $status;
    }

    /**
     * Возвращает название роли
     */
    public function getRoleName()
    {
        $role = 'Нет роли';
        switch ($this->role) {
            case self::ROLE_USER:
                $role = 'Пользователь';
                break;
            case self::ROLE_MODERATOR:
                $role = 'Модератор';
                break;
            case self::ROLE_ADMIN:
                $role = 'Администратор';
                break;
        }
        return $role;
    }

    /**
     * @inheritDoc
     */
    public static function getList($id = 'id', $title = 'email')
    {
        return parent::getList($id, $title);
    }


    /**
     * @return ActiveQuery
     */
    public function getChildrenQuery()
    {
        return $this->hasMany(Child::className(),['parent_id' => 'id']);
    }
//        public function logout($destroySession = true)
//    {
//        $identity = $this->getIdentity();
//        if ($identity !== null && $this->beforeLogout($identity)) {
//            $this->switchIdentity(null);
//            $id = $identity->getId();
//            $ip = Yii::$app->getRequest()->getUserIP();
//            Yii::info("User '$id' logged out from $ip.", __METHOD__);
//            if ($destroySession && $this->enableSession) {
//                Yii::$app->getSession()->destroy();
//            }
//            $this->afterLogout($identity);
//        }
//
//        return $this->getIsGuest();
//    }

    public function getPayStatus()
    {
        $children = $this->getChildrenQuery()->all();
        $payments = Payment::find()->where(['user_id' => $this->id, 'processed' => false])->all();
        $state = false;
        foreach ($payments as $payment)
        {
            $state = $payment->savePaymentResult();
        }
        //Записываем в куки информациб об изменении оплаты, для работы целей в метрике (см. views/template/_goal_events.php)
        if($state) Yii::$app->session->setFlash('payment','status changed');

        foreach ($children as $child)
        {
            if($child->payStatus == Child::PAY_STATUS_EXPIRED) return User::PAY_STATUS_EXPIRED;
        }
        return User::PAY_STATUS_PAYED;
    }

    public function getAllPromoCodes() {
        $promocodes = [];
        $payments = Payment::find()->where(['user_id' => $this->id, 'status' => 'succeeded'])->all();

        foreach ($payments as $payment)
        {
            if($payment->promo_code && !array_search($payment->promo_code, $promocodes)) {
                $promocodes[] = $payment->promo_code;
            }
        }

        return implode(", ", $promocodes);
    }

    public function getAllPaymentsData($field) {


        $payments = Payment::find()->where(['user_id' => $this->id])->all();

        $data_payments_id = [];
        $data_date_create = [];
        $data_payments_price = [];
        $response = '';

        foreach ($payments as $item)
        {
            if ($item->status == 'succeeded') {
                $data_payments_id[] = $item->payment_id;
                $data_date_create[] = date('d.m.Y H:i', $item->created_at);
                if (isset($item->amount)) {
                    $data_payments_price[] = $item->amount;
                }
            }

        }

        switch ($field) {
            case 'id':
                $response = implode(", ", $data_payments_id);
                break;
            case 'price':
                $response = implode(", ", $data_payments_price);
                break;
            case 'date':
                $response = implode(", ", $data_date_create);
                break;
        }

        return $response;
    }

}