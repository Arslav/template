<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Настройки, хранящиеся в БД
 */
class SiteInfo extends Model
{
    public $aboutProjectName;
    public $aboutProjectTitle;
    public $aboutProjectImage1;
    public $aboutProjectImage2;
    public $aboutProjectImage3;
    public $aboutProjectContent;

    public $locationName;
    public $locationTitle;
    public $locationText;
    public $locationImage;

    public $advantageName;
    public $advantageTitle;
    public $advantageImage1;
    public $advantageTitle1;
    public $advantageText1;
    public $advantageImage2;
    public $advantageTitle2;
    public $advantageText2;
    public $advantageImage3;
    public $advantageTitle3;
    public $advantageText3;
    public $advantageImage4;
    public $advantageTitle4;
    public $advantageText4;
    public $advantageImage5;
    public $advantageTitle5;
    public $advantageText5;

    public $apartmentsName;
    public $apartmentsTitle;

    public $videoName;
    public $videoTitle;
    public $videoTranslationLink;

    public $aboutName;
    public $aboutTitle;
    public $aboutContent;
    public $aboutSberText;

    public $imageAboutImage1;
    public $imageAboutImage2;
    public $imageAboutImage3;
    public $imageAboutImage4;
    public $imageAboutImage5;
    public $imageAboutImage6;

    public function rules()
    {
        return [
            [[
                'aboutProjectName',
                'aboutProjectTitle',
                //'aboutProjectImage1',
                //'aboutProjectImage2',
                //'aboutProjectImage3',
                'aboutProjectContent',
                'locationName',
                'locationTitle',
                'locationText',
                //'locationImage',
                'advantageName',
                'advantageTitle',
                //'advantageImage1',
                'advantageTitle1',
                'advantageText1',
                //'advantageImage2',
                'advantageTitle2',
                'advantageText2',
                //'advantageImage3',
                'advantageTitle3',
                'advantageText3',
                //'advantageImage4',
                'advantageTitle4',
                'advantageText4',
                //'advantageImage5',
                'advantageTitle5',
                'advantageText5',
                'apartmentsName',
                'apartmentsTitle',
                'videoName',
                'videoTitle',
                'videoTranslationLink',
                'aboutName',
                'aboutTitle',
                'aboutContent',
                'aboutSberText',
            ], 'string'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'aboutProjectName' => 'Название блока',
            'aboutProjectTitle' => 'Заголовок',
            'aboutProjectImage1' => 'Изображение 1',
            'aboutProjectImage2' => 'Изображение 2',
            'aboutProjectImage3' => 'Изображение 3',
            'aboutProjectContent' => 'Текст',
            'locationName' => 'Название блока',
            'locationTitle' => 'Заголовок',
            'locationText' => 'Текст',
            'locationImage' => 'Изображение',
            'advantageName' => 'Название блока',
            'advantageTitle' => 'Заголовок',
            'advantageImage1' => 'Изображение (Преимущество 1)',
            'advantageTitle1' => 'Заголовок (Преимущество 1)',
            'advantageText1' => 'Текст (Преимущество 1)',
            'advantageImage2' => 'Изображение (Преимущество 2)',
            'advantageTitle2' => 'Заголовок (Преимущество 2)',
            'advantageText2' => 'Текст (Преимущество 2)',
            'advantageImage3' => 'Изображение (Преимущество 3)',
            'advantageTitle3' => 'Заголовок (Преимущество 3)',
            'advantageText3' => 'Текст (Преимущество 3)',
            'advantageImage4' => 'Изображение (Преимущество 4)',
            'advantageTitle4' => 'Заголовок (Преимущество 4)',
            'advantageText4' => 'Текст (Преимущество 4)',
            'advantageImage5' => 'Изображение (Преимущество 5)',
            'advantageTitle5' => 'Заголовок (Преимущество 5)',
            'advantageText5' => 'Текст (Преимущество 5)',
            'apartmentsName' => 'Название блока',
            'apartmentsTitle' => 'Заголовок',
            'videoName' => 'Название блока',
            'videoTitle' => 'Заголовок',
            'videoTranslationLink' => 'Ссылка на онлайн трансляцию',
            'aboutName' => 'Название блока',
            'aboutTitle' => 'Заголовок',
            'aboutContent' => 'Текст',
            'aboutSberText' => 'Поясняющий текст об иконке "Сбербанк"',

            'imageAboutImage1' => 'Изображение в сладере 1',
            'imageAboutImage2' => 'Изображение в сладере 2',
            'imageAboutImage3' => 'Изображение в сладере 3',
            'imageAboutImage4' => 'Изображение в сладере 4',
            'imageAboutImage5' => 'Изображение в сладере 5',
            'imageAboutImage6' => 'Изображение в сладере 6',
        ];
    }

    /**
     * Сохранение файла на сервере
     */
    public function saveFile()
    {

        $web = Yii::getAlias('@webroot');

        $dir = '/upload/settings/';
        \yii\helpers\BaseFileHelper::createDirectory($web . $dir);

        foreach ($this->attributes as $attr => $value) {

            $uploadFile = \yii\web\UploadedFile::getInstance($this, $attr);

            if (is_object($uploadFile)) {

                $name = $attr . date('m-Y_His');
                $ext = $uploadFile->getExtension();

                $filePath = $dir . $name . '.' . $ext;

                $uploadFile->saveAs($web . $filePath);
                $this->{$attr} = $filePath;
            }
        }
    }
}
