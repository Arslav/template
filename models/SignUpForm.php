<?php


namespace app\models;


use app\components\UnisenderHelper;
use Yii;
use yii\base\Exception;
use yii\base\Model;

class SignUpForm extends Model
{
    public $email;
    public $name;
    public $password;
    public $password_repeat;
    public $privacy_policy;
    public $check;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['name', 'password', 'password_repeat', 'email'], 'required'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'Passwords dont match')],
            ['email', 'email', 'message' => 'Некорректный адрес электронной почты'],
            //['email', 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email'],
            ['privacy_policy', 'required', 'requiredValue' => 1, 'message' => 'Необходимо принять условия политики конфиденциальности'],
            ['password', 'string', 'length' => [6]],
            [['check'], 'in', 'range' => [14]],
            [['check'], 'required'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'password' => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Repeat Password'),
            'email' => Yii::t('app', 'Email')
        ];
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function signUp()
    {
        if ($this->validate()) {
            $_user = User::find()->where(['email' => $this->email])->one();
            if ($_user != null && $_user->status == User::STATUS_ACTIVE) {
                $this->addError('email', 'Пользователь с таким адресом электронной почты уже существует');
                return false;
            }
            $user = new User();
            if ($_user) $user = $_user;
            $user->name = $this->name;
            $user->setPassword($this->password);
            $user->email = $this->email;
            $user->auth_key = '';
            $user->role = User::ROLE_USER;
            $user->status = User::STATUS_DELETED;
            $user->validation_token = Yii::$app->security->generateRandomString();
            $user->save();
            try {
                Yii::$app->mailer->compose('validate', ['user' => $user])
                    ->setTo($this->email)
                    ->setFrom([Yii::$app->settings->get('SiteSettings', 'fromEmail') => 'Талантикум'])
                    ->setSubject('Подтверждение регистрации')
                    ->send();
            } catch (\Exception $exc) {
                Yii::error($exc);
            }
            return true;
        }
        return false;
    }
}