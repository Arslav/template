<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Настройки, хранящиеся в БД
 */
class SiteSettings extends Model {

	public $email;
    public $address;
    public $workTime;
    public $phone;
    public $centerMap;

	public $homePageTitle;
	public $homePageKeywords;
	public $homePageDescription;
	public $logoImg;
	public $privacyLink;

    public $sendEmailsTo;
	public $smtpHost;
	public $smtpPort;
	public $smtpAuth;
	public $smtpUsername;
	public $smtpPassword;
	public $smtpSecure;
	public $fromEmail;

	public $wa_link;

	public function rules()
	{
            return [
                [['email'], 'string', 'max' => 255],
                [['homePageTitle', 'homePageKeywords', 'homePageDescription',], 'string', 'max' => 255],
                [['sendEmailsTo', 'smtpHost', 'smtpUsername', 'smtpPassword'], 'string'],
                [['smtpPort'], 'integer', 'min'=>1, 'max'=>65536],
                [['smtpAuth'], 'in', 'range'=>[0,1]],
                [['smtpSecure'], 'in', 'range'=>['ssl', 'tls']],
                [['fromEmail'], 'string', 'max' => 255],
                [['fromEmail'], 'email'],
                ['logoImg', 'string'],
                ['privacyLink', 'string'],
                ['address', 'string'],
                ['workTime', 'string'],
                ['phone', 'string'],
                ['centerMap', 'string'],
                ['wa_link', 'string'],
            ];
	}
	
    public function attributeLabels()
    {
        return [
            'homePageTitle' => Yii::t('app', 'Home page title'),
            'homePageKeywords' => Yii::t('app', 'Home page keywords'),
            'homePageDescription' => Yii::t('app', 'Home page description'),
            'privacyLink' =>  Yii::t('app','Link'),
            'sendEmailsTo' => Yii::t('app', 'Sends email to'),
            'smtpHost' => Yii::t('app', 'Host'),
            'smtpPort' => Yii::t('app', 'Port'),
            'smtpAuth' => Yii::t('app', 'Need authorization'),
            'smtpUsername' => Yii::t('app', 'Username'),
            'smtpPassword' => Yii::t('app', 'Password'),
            'smtpSecure' => Yii::t('app', 'Security'),
            'fromEmail' => Yii::t('app', 'From email'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'centerMap' => Yii::t('app', 'Center Map'),
            'wa_link'=> Yii::t('app', 'Link'),
            'workTime' => Yii::t('app','Worktime'),
        ];
    }

    /**
     * Сохранение файла на сервере
     */
    public function saveFile(){

        $web = Yii::getAlias('@webroot');

        $dir = '/upload/settings/';
        \yii\helpers\BaseFileHelper::createDirectory($web . $dir);

        foreach ($this->attributes as $attr => $value) {

            $uploadFile = \yii\web\UploadedFile::getInstance($this, $attr);

            if (is_object($uploadFile)) {

                $name = $attr . date('m-Y_His');
                $ext = $uploadFile->getExtension();

                $filePath = $dir . $name . '.' . $ext;

                $uploadFile->saveAs($web . $filePath);
                $this->{$attr} = $filePath;
            }
        }
    }
}
